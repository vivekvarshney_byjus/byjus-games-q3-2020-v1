﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bolt;
using Ludiq;

public class Randomize : MonoBehaviour
{
    GameObject parentObject;
    //public int numberOfWindowRequired = 4;
    public bool randomizeWindow = true;
    public bool haveBadGuys = true;
    public bool haveGoodGuys = true;
    public int value;
    public int childValue;
    public int numberOfBadGuys;
    // Start is called before the first frame update
    void Start()
    {
        parentObject = this.gameObject;
        Variables.ActiveScene.Set("badGuysleftinLevel", Variables.ActiveScene.Get<int>("badGuysleftinLevel") + numberOfBadGuys);
        //Random r = new Random();
        //randomizeLevel();
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void randomizeLevel()
    {
        parentObject.transform.SetAsLastSibling();
        foreach (Transform child in parentObject.transform)
        {
            //Debug.Log("numberOfWindowRequired " + numberOfWindowRequired);
            bool isThereMinEnemy = false;
            value = Random.Range((int)0, (int)1000);

            if (!randomizeWindow)
                value = 1;

            Debug.Log("Random selected" + value);
            if (value % 2 == 1)
            {
                Debug.Log("Window will be active for " + parentObject.name + ":" + child.name);
                child.gameObject.SetActive(true);

                Debug.Log(child.name + " : active");
                childValue = Random.Range((int)0, (int)1000);

                if (childValue % 3 == 0)
                {
                    child.GetComponent<Button>().enabled = false;
                    child.GetChild(0).gameObject.SetActive(false);
                    child.GetChild(1).gameObject.SetActive(false);
                }
                else if (childValue % 3 == 1 && haveGoodGuys)
                {
                    child.GetChild(0).gameObject.SetActive(true);
                    //isThereMinEnemy = true;
                }
                else if (childValue % 3 == 2 && haveBadGuys)
                {
                    child.GetChild(1).gameObject.SetActive(true);
                    numberOfBadGuys++;

                    //isThereMinEnemy = true;
                }
                //numberOfWindowRequired--;
            }
            else
            {
                child.gameObject.SetActive(false);
                Debug.Log(child.name + " : inactive");
            }

            if (isThereMinEnemy)
            {
                //randomizeLevel();
                Debug.Log("isThereMinEnemy " + isThereMinEnemy);
            }

        }
        Variables.ActiveScene.Set("badGuysleftinLevel", Variables.ActiveScene.Get<int>("badGuysleftinLevel") + numberOfBadGuys);
    }
    public void resetWindow()
    {
        foreach (Transform child in parentObject.transform)
        {
            child.gameObject.SetActive(true);
            numberOfBadGuys = 0;
            child.GetComponent<Button>().enabled = true;
            child.GetChild(0).gameObject.SetActive(false);
            child.GetChild(0).GetComponent<Animator>().enabled = true;
            child.GetChild(0).GetComponent<Image>().sprite = null;
            child.GetChild(1).gameObject.SetActive(false);
            child.GetChild(1).GetComponent<Animator>().enabled = true;
            child.GetChild(1).GetComponent<Image>().sprite = null;
            //randomizeLevel();
        }
    }
}

